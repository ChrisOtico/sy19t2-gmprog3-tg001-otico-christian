﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CameraChange : MonoBehaviour
{
    
    public Transform MonsterCamPos;
    public Transform PlayerCamPos;
    public Transform NpcCamPos;

    public void ChangeCameraPlayer()
    {
        transform.position = PlayerCamPos.localPosition;
    }
 
    public void ChangeCameraMonster()
    {
        transform.position = MonsterCamPos.localPosition;
    }

    public void ChangeCameraNPC()
    {
        transform.position = NpcCamPos.localPosition;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCanvas : MonoBehaviour
{
    public GameObject playerCanvas;
    public GameObject monsterCanvas;
    public GameObject npcCanvas;



    // Start is called before the first frame update
    public void playerCan()
    {
        playerCanvas.SetActive(true);
        monsterCanvas.SetActive(false);
        npcCanvas.SetActive(false);

  
    
    }

    public void monsterCan()
    {
        playerCanvas.SetActive(false);
        monsterCanvas.SetActive(true);
        npcCanvas.SetActive(false);

  
    }

    public void npcCan()
    {
        playerCanvas.SetActive(false);
        monsterCanvas.SetActive(false);
        npcCanvas.SetActive(true);
     

    }

}

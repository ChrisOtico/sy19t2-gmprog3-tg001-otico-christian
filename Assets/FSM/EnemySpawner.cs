﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public GameObject[] spawnEnemy;
    public Transform spawnPosition;
    Vector3 randomLocation;
    private float radius = 5.0f;
    private float timer = 3;



    //TIMER
    //Coroutine
    CharMovement charMove;
    MonsterAi monsterScript;

    void Start()
    {
        StartCoroutine(SpawnEnemy());
    }

    IEnumerator SpawnEnemy()
    {
        yield return new WaitForSeconds(timer);
        randomLocation = Random.insideUnitSphere * radius;
        randomLocation.y = 0.0f;
        int spawnObjectIndex = Random.Range(0, spawnEnemy.Length);
        float spawnRadius = radius;
        transform.position = Random.insideUnitSphere * spawnRadius;
        if (GameObject.FindGameObjectsWithTag("Enemy").Length < spawnEnemy.Length)
        {

            Debug.Log(spawnEnemy.Length);

            Instantiate(spawnEnemy[spawnObjectIndex], spawnPosition.position + randomLocation, spawnEnemy[spawnObjectIndex].transform.rotation);
            StartCoroutine(SpawnEnemy());


        }

    }

}

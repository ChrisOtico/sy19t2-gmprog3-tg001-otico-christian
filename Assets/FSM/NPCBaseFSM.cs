﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class NPCBaseFSM : StateMachineBehaviour
{
    // Start is called before the first frame update
    public GameObject NPC;
    public GameObject opponent;
    //public float speed = 2.0f;
    //public float rotSpeed = 0.5f;
    public float accuracy = 2.0f;
    public NavMeshAgent agent;
    private float chaseSpeed = 3.0f;
    private float accelareteSpeed = 3.0f;

    public Transform target;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        NPC = animator.gameObject;
        opponent = NPC.GetComponent<MonsterAi>().GetPlayer(); //real one
        agent = NPC.GetComponent<NavMeshAgent>();


    }

    public void changeSpeed()
    {
        agent.speed = chaseSpeed;
    }
    public void changeAccelerateSpeed()
    {
        agent.acceleration = accelareteSpeed;
    }

}

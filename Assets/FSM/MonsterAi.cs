﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
public class MonsterAi : MonoBehaviour
{
    private int _maxHealth = 10;
    private int _currentHealth;
    private int _damage = 2;
    private int _Vitality = 10;
    private int _Str = 2;
    public HeathBarScript healthBar;

    Animator anim;
    public GameObject player;


    Player playerScript;
    CharMovement charMovement;
    EnemySpawner enemySpawner;
    public int xpPoints = 2;
    [SerializeField]
    public Text Exp;

    public bool isDead;
    void Start()
    {
        _maxHealth = _Vitality * 10;
        _currentHealth = _maxHealth;
        healthBar.SetMaxHealth(_maxHealth);
        anim = GetComponent<Animator>();
        playerScript = player.GetComponent<Player>();
    
        _damage = _Str * 4;
     
  

        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
      
        anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
        ImDead();
       

    }


    public void DamageTo()
    {

        playerScript = playerScript.gameObject.GetComponent<Player>();

        if (playerScript.currHealth >= 0)
        {
            playerScript.currHealth -= _damage;
            playerScript.healthBar.setHealth(playerScript.currHealth);
         
     
        }
    }

    public void ImDead()
    {
        isDead = true;
        if (_currHealth <= 0)
        {
            giveExp();
            Destroy(gameObject, 2);
            anim.SetBool("isDead", true);
            playerScript.StopEffect();
      
   
        }
        
    }

    public void giveExp()
    {
        
        playerScript.levelUpBar.value += xpPoints;
        //UPDATE EXP TEXT      
        xpPoints = playerScript.exp;




        //update xp of player

    }
    public void StopAttacking()
    {
        CancelInvoke("DamageTo");
    }

    public void StartAttacking()
    {
      
        anim.Play("Attack");
    }

    public int attackPower
    {
        get { return _damage; }
        set { _damage = value; }
    }
    public int maximumHp
    {
        get { return _maxHealth; }
        
        set { _maxHealth = value; }      
    }

    public int _currHealth
    {
        get { return _currentHealth; }
        set { _currentHealth = value; }
    }
    public GameObject GetPlayer()
    {
        return player;
    }
    // Start is called before the first frame update
  

  
}

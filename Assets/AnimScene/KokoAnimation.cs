﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KokoAnimation : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator anim;

    private const string KK_Idle_ANIMATION = "KK_Idle";
    private const string KK_Attack_ANIMATION = "KK_Attack";
    private const string KK_Run_ANIMATION = "KK_Run";
    private const string KK_Run_No_ANIMATION = "KK_Run_No";
    private const string KK_Attack_Standy_ANIMATION = "KK_Attack_Standy";
    private const string KK_Combo_ANIMATION = "KK_Combo";
    private const string KK_PutBlade_ANIMATION = "KK_PutBlade";
    private const string KK_DrawBlade_ANIMATION = "KK_DrawBlade";
    private const string KK_Damage_ANIMATION = "KK_Damage";
    private const string KK_Skill_ANIMATION = "KK_Skill";
    private const string KK_Dead_ANIMATION = "KK_Dead";

    //SKILL
    public void AreaAttack()
    {
        ;
    }
    //SKILL
    //ATTACK
    public void StartEffect()
    {
        ;
    }
    public void DamageTo()
    {
        ;
    }
    public void StopEffect()
    {
        ;
    }
    //ATTACK
    public void AnimateIdle()
    {
        Animate(KK_Idle_ANIMATION);
    }
    public void AnimateAttack()
    {
        Animate(KK_Attack_ANIMATION); 
    }
    public void AnimateRun()
    {
        Animate(KK_Run_ANIMATION);
    }    
    public void AnimateRunAttack()
    {
        Animate(KK_Run_No_ANIMATION);
    }
    public void AnimateStandy()
    {
        Animate(KK_Attack_Standy_ANIMATION); 
    }
    public void AnimateCombo()
    {
        Animate(KK_Combo_ANIMATION);
    }
    public void AnimateDrawBlade()
    {
        Animate(KK_DrawBlade_ANIMATION);
    }
    public void AnimatePutBlade()
    {
        Animate(KK_PutBlade_ANIMATION);
    }
    public void AnimateDamage()
    {
        Animate(KK_Damage_ANIMATION);
    }
    public void AnimateSkill()
    {
        Animate(KK_Skill_ANIMATION);
    }
    public void AnimateDead()
    {
        Animate(KK_Dead_ANIMATION);
    }

    void Start()
    {
        anim = GetComponent<Animator>();
    }
    void Animate(string boolName)
    {
        DisableOtherAnims(anim, boolName);
        anim.SetBool(boolName, true);
    }
    void DisableOtherAnims(Animator anim, string animation)
    {
        foreach(AnimatorControllerParameter parameter in anim.parameters)
        {
            if(parameter.name != animation)
            {
                anim.SetBool(parameter.name, false); 
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public GameObject player;     
    private Vector3 offset;
    public float smoothSpeed = 10f;

    // Use this for initialization
    void Start()
    {     
        offset = transform.position - player.transform.position;
    }

    void LateUpdate()
    { 
        Vector3 desiredPos = player.transform.position + offset;
        Vector3 smoothedPos = Vector3.Lerp(transform.position, desiredPos, smoothSpeed * Time.deltaTime);
        transform.position = smoothedPos;
    }
}

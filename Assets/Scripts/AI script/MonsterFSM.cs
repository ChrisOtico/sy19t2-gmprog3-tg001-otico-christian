﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterFSM : Singleton<MonsterFSM>
{


    [SerializeField]
    detector myDetector;

    enum MonsterState
    {
        Chase,
        Attack,
        Patrol
    }
    // Start is called before the first frame update
    [SerializeField] MonsterState curState;
    void Start()
    {
        curState = MonsterState.Patrol;
        myDetector = GetComponentInChildren<detector>();
    }
    void Update()
    {
        switch(curState)
        {
            case MonsterState.Chase:
                ChaseUpdate();
                break;
            case MonsterState.Attack:
                AttackUpdate();
                break;
            case MonsterState.Patrol:
                PatrolUpdate();
                break;
        }

    }

    public void ChaseUpdate()
    {
        if(myDetector.detectTarget != null)
        {
            Debug.Log("PLAYER DETECTED");
        }
        if(myDetector.detectTarget == null)
        {
            PatrolUpdate();
        }
    }

    public void AttackUpdate()
    {
        
    }

    public void PatrolUpdate()
    {
        if(myDetector.detectTarget == null)
        {
            Debug.Log("Goblin Patrolling");
        }
    }



}

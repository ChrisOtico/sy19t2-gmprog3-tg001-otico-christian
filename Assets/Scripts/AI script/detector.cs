﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detector : Singleton<detector>
{
    public Transform detectTarget;
    // Start is called before the first frame update
    //gameibject not transform
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("PLAYER ENTERED");
            detectTarget = other.gameObject.transform;
            return;
        }
        //else
        //{
        //    Debug.Log("EXIT");
        //    detectTarget = other.gameObject.transform;
         
        //}
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("PLAYER EXITED");
            detectTarget = other.gameObject.transform;
            return;
        }
    }
}

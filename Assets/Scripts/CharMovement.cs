﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharMovement : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public NavMeshAgent mNavmeshAgent;
    public LayerMask Ground;
    public LayerMask Enemy;


    //Waypoint and Movement
    public GameObject pointer;
    public GameObject enemyPointer;
    public List<GameObject> cloneObjectList;
    public List<GameObject> cubes = new List<GameObject>();
    int maxCubes = 1;
    private int maxPointer = 2;


    //Animator
    public Animator mAnimator;
    public bool mRunning = false;


    //EnemyDetection
    public GameObject raycastObject;
    public GameObject selectedUnit = null;



    Player playerStats;
    [SerializeField]

    MonsterAi monsterStats;

    private GameObject enemy;

    public Transform MyTarget { get; set; }

    void Start()
    {

        mNavmeshAgent = GetComponent<NavMeshAgent>();
        mAnimator = GetComponent<Animator>();
        
    }
    // Update is called once per frame

    void FixedUpdate()
    {
        //Debug.DrawLine(ray.origin, hit.point); FOR DETECTING RAYCAST ON MOUSE
        //Debug.Log(hit.point);

        //playerStats.DetectEnemy();
        //DetectEnemy();       
        SelectTarget();
          
        MoveChar();
        //Run();
        distanceChecker();
     
     
     

    }
    public void MoveChar() // MOVE
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, 100, Ground))
            {
                mNavmeshAgent.isStopped = false;
                Debug.DrawLine(ray.origin, hit.point);
                mNavmeshAgent.destination = hit.point;
                GameObject cloneObject = Instantiate(pointer, mNavmeshAgent.destination, Quaternion.identity);
                cloneObjectList.Add(cloneObject);
                DeselectTarget();              
            }
        }
    }
    public void distanceChecker()
    {

        Run();
        foreach (GameObject cloneObject in cloneObjectList.ToArray())
        {
            //mRunning = false;
            if (cloneObject != null)
            {
                float distanceBeteenObj = Vector3.Distance(cloneObject.transform.position, mNavmeshAgent.transform.position);
                //float DistanceBetweenObjects = (cloneObject.transform.position - mNavmeshAgent.transform.position).sqrMagnitude;
                if (MyTarget != null || distanceBeteenObj <= 1 || cloneObjectList.Count == maxPointer)
                {
                   
                    cloneObjectList.Remove(cloneObject);
                    Destroy(cloneObject);
                }                   
            }
        }
    }


    public void SelectTarget()
    {            
        if (MyTarget != null )
        {
           
            NormalAttack();
            float distancefromenemy = Vector3.Distance(mNavmeshAgent.transform.position, MyTarget.transform.position);
            mNavmeshAgent.SetDestination(MyTarget.transform.position);
            //if(distancefromenemy <= 2)
            //{
            //    mAnimator.SetBool("Running", false);
            //}
            if (cubes.Count < maxCubes)
            {               
                GameObject pointerOfEnemy = Instantiate(enemyPointer, MyTarget.gameObject.transform.position, Quaternion.identity);
                //calculate target direction              
                enemyPointer.transform.position = MyTarget.transform.position;
                //FOLLOW ENEMY                 
                cubes.Add(pointerOfEnemy);
                Debug.Log("KALABAN TO");
            
            }                   
        }
    }   
    public void NormalAttack()
    {
        playerStats = gameObject.GetComponent<Player>();
        float distanceToEnemy = Vector3.Distance(mNavmeshAgent.transform.position, MyTarget.transform.position);
        if(distanceToEnemy <= 2)
        {
            playerStats.StartEffect();
            gameObject.GetComponent<NavMeshAgent>().velocity = Vector3.zero;
            //mAnimator.SetBool("Running", false);
            mNavmeshAgent.transform.LookAt(MyTarget.transform);        
        }
     
    }
    public void Run()
    {     
        if (mNavmeshAgent.remainingDistance <= mNavmeshAgent.stoppingDistance)
        {
            mRunning = false;        
        }
        else
        {
            mRunning = true;          
        }
        mAnimator.SetBool("Running", mRunning); //Running set to false. when target reached point
    }
   
    public void DeselectTarget()
    { 
        MyTarget = null;
        foreach (GameObject pointerOfEnemy in cubes)
        {
            Destroy(pointerOfEnemy);
        }
        cubes.Clear();
    }

    //public void AttackEnemy()
    //{
    //    RaycastHit objectHit;
    //    Vector3 fwd = raycastObject.transform.TransformDirection(Vector3.forward);
    //    monsterStats = gameObject.GetComponent<MonsterAi>();
    //    Debug.DrawRay(raycastObject.transform.position, fwd * rayCastLength, Color.green);
    //    if (Physics.Raycast(raycastObject.transform.position, fwd, out objectHit, rayCastLength))
    //    {
    //        //do something if hit object ie
    //        if (objectHit.collider.tag == "Enemy")
    //        {
    //            selectedUnit = objectHit.collider.gameObject;

    //            Debug.Log(objectHit.transform.gameObject.name);
    //            //damage enemy for testing
    //            playerStats.DamageTo();
    //        }
    //    }

    //}






    
}




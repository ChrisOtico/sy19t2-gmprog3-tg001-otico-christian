﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelUpScript : MonoBehaviour
{
   public int level;
   public int exp;
   public int expNeedToLvlUp;

    public Slider levelUpBar;
    public Text currentLevel;

    // Start is called before the first frame update
    void Start()
    {
        level = 0;
        exp = 0;
        expNeedToLvlUp = 10;

        levelUpBar.value = exp;
        levelUpBar.maxValue = expNeedToLvlUp;

        currentLevel.text = "Level : 0";
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.F))
        {
            exp += 2;
            levelUpBar.value = exp;
        }
        if (levelUpBar.value >= levelUpBar.maxValue)
        {
            increaseLevel();
        }
    }

    void increaseLevel()
    {
        exp = 0;
        levelUpBar.value = exp;

        expNeedToLvlUp += 10;
        levelUpBar.maxValue = expNeedToLvlUp;

        level += 1;
        currentLevel.text = "Level : " + level.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Player : MonoBehaviour
{
    public int level;
    public int exp;
    public int expNeedToLvlUp;
    private int pointsToSpend;
    public Slider levelUpBar;

    public Text currentLevel;
    [SerializeField]
    private Text HpBarTEXT;
    [SerializeField]
    private Text AttackText;
    [SerializeField]
    private Text StrText;
    [SerializeField]
    private Text VitText;
    [SerializeField]
    public Text ExpText;
    [SerializeField]
    private Text PointsToSpendText;

    private int _maxHealth = 10; //curr maxhealth
    private int mhp;
    private int _currentHealth;
    private int _damage;
    private int curr_Vitality = 10;
    private int vit;
    private int curr_Str = 2;
    private int str;

    public HeathBarScript healthBar;
    MonsterAi monsterAiScript;
    public bool canAttack = true;

    Animator mAnim;
    [SerializeField]
    CharMovement charMovement;


    //SET LEVEL to 1
    void Start()
    {
        _maxHealth = curr_Vitality * 10; 
        _currentHealth = _maxHealth;
        healthBar.SetMaxHealth(_maxHealth);

        vit = curr_Vitality;
        str = curr_Str;
        mAnim = GetComponent<Animator>();        
        _damage = curr_Str * 10;

        level = 1;
        exp = 0;
        expNeedToLvlUp = 10;

        levelUpBar.value = exp;
        levelUpBar.maxValue = expNeedToLvlUp;
        currentLevel.text = "Level : 1";      
    }
    void FixedUpdate()
    {
        Debug.Log(exp);
        StatsView();
        //ADD SKILL POINT 1 if (level up) Add skill point
        if (Input.GetKeyDown(KeyCode.F))
        {
            exp += 2;
            levelUpBar.value = exp;
        }
        //}
        if (levelUpBar.value >= levelUpBar.maxValue)
        {
            increaseLevel();
        }
        Debug.Log(level);
    }


    public void DamageTo()
    {
        if (charMovement.MyTarget != null)
        {
            monsterAiScript = charMovement.MyTarget.gameObject.GetComponent<MonsterAi>();
            if (monsterAiScript._currHealth >= 0)
            {
                monsterAiScript._currHealth -= _damage;
                monsterAiScript.healthBar.setHealth(monsterAiScript._currHealth);
            }
        }

    }

    void increaseLevel()
    {
        exp = 0;
        levelUpBar.value = exp;
        expNeedToLvlUp += 10;
        levelUpBar.maxValue = expNeedToLvlUp;
        level += 1;
        maximumHp += 5;
        // maximumHp = 100
        //if level up curr maximumHp += 5
        _currentHealth = maximumHp;
        healthBar.SetMaxHealth(maximumHp);
        //ATTACk
        _damage += Random.Range(1,4);
        //STR
        curr_Str += Random.Range(2, 5);
        //VIT
        curr_Vitality += Random.Range(3, 10);

        pointsToSpend += 1;
    }

    private void StatsView()
    {
        HpBarTEXT.text = "HP: " + maximumHp.ToString() + " / " + currHealth.ToString();
        AttackText.text = "Attack: " + _damage;
        StrText.text = "STR: " + curr_Str;
        VitText.text = "VIT: " + curr_Vitality;
        ExpText.text = "EXP: " + exp.ToString();
        PointsToSpendText.text = "" + pointsToSpend;
    }

    public void StartEffect()
    {
        mAnim.SetBool("isAttacking", true);
    }

    public void StopEffect()
    {
        mAnim.SetBool("isAttacking", false);
        CancelInvoke("DamageTo");
        //mAnim.ResetTrigger("isAttack");        

    }


    public int attackPower
    {
        get { return _damage; }
        set { _damage = value; }
    }

    public int currHealth
    {
        get { return _currentHealth = Mathf.Clamp(_currentHealth, 0, _maxHealth); }
        set { _currentHealth = value; }
   
             
    }
    public int maximumHp
    {
        get
        {
            return _maxHealth;
        }
        set
        {           
            _maxHealth = value;
        }
    }
    public void setStrength(int amount)
    {
        if(amount > 0 && pointsToSpend > 0)
        {
            curr_Str += amount;
            pointsToSpend -= 1;
        }
        else if(amount < 0 && curr_Str > str)//if error here
        {
            curr_Str += amount;
            pointsToSpend += 1;
        } 
    }
    public void setVitality(int amount)
    {
        if (amount > 0 && pointsToSpend > 0)
        {
            curr_Vitality += amount;
            pointsToSpend -= 1;
        }
        else if (amount < 0 && curr_Vitality > vit)//if error here
        {
            curr_Vitality += amount;
            pointsToSpend += 1;
        }
    }
 

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private CharMovement charMovement;

    public LayerMask Enemy;
    // Start is called before the first frame update
    private Transform target;
    // Update is called once per frame
    void Update()
    {
        clickTarget();

    }

    private void clickTarget()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100, Enemy))
            {
                if(hit.collider != null)
                {
                    if(hit.collider.tag == "Enemy")
                    {

                        charMovement.MyTarget = hit.transform;
                       

                    }
                  
                }
                else
                {
                    //Detarget
                    charMovement.MyTarget = null;
                    
                }
           
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ExpScript : MonoBehaviour
{
    Player playerScript;
    public Slider expSlider;
    MonsterAi monsScript;
    // Start is called before the first frame update

    
    public void SetMaxExp(int exp)
    {
        expSlider.maxValue = exp;
        expSlider.value = exp;

    }
    // Update is called once per frame
    public void setExp(int exp)
    {
      
        expSlider.value = exp;
    }
}
